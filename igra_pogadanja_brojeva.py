import random

guesses_made = 0

name = input('Pozdrav! Kako se zoveš?\n')

number = random.randint(1, 20)
print ('Dakle, {0}, Mislim o broju izmedu 0 i 40.'.format(name))

while True:

    guess = int(input('Probaj pogoditi sigurno ćeš pogrijesiti: '))

    guesses_made += 1

    if guess < number:
        print ('Premalo.')

    if guess > number:
        print ('Previse.')

    if guess == number:
      
        break

if guess == number:
    print ('Svaka cast, {0}! Pogodio si iz {1} pokusaja!'.format(name, guesses_made))
else:
    print ('Neces razbojnice nisi pogodio broj koji je {0}'.format(number))